package desafio.b2w.api.controller;

import desafio.b2w.api.Aplicacao;
import desafio.b2w.api.entity.Planeta;
import desafio.b2w.api.repository.PlanetaRepository;
import desafio.b2w.api.service.PlanetaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Aplicacao.class})
@AutoConfigureMockMvc
public class PlanetaControllerTest {

    @MockBean
    private PlanetaRepository planetaRepository;

    @MockBean
    private PlanetaService planetaService;

    @Autowired
    private MockMvc mvc;

    @Test
    public void obterPlanetas_FiltrarPorNome() throws Exception {

        Planeta planeta = new Planeta("utini", "clima", "terreno");

        when(planetaRepository.findByNomeIgnoreCase(planeta.getNome())).thenReturn(planeta);

        mvc.perform(get("/planetas")
                .param("nome", planeta.getNome()))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nome", is(planeta.getNome())));
    }


    @Test
    public void obterPlanetas_Todos() throws Exception {

        Planeta planeta1 = new Planeta("utini", "clima", "terreno");
        Planeta planeta2 = new Planeta("wookie", "clima", "terreno");

        when(planetaRepository.findAll()).thenReturn(Arrays.asList(new Planeta[]{planeta1, planeta2}));

        mvc.perform(get("/planetas"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].nome", is(planeta1.getNome())))
                .andExpect(jsonPath("$[1].nome", is(planeta2.getNome())));
    }

    @Test
    public void obterPlanetas_SeNaoHouverPlanetaFiltrado_RetornarNotFound() throws Exception {
        Planeta planeta = new Planeta("utini", "clima", "terreno");

        when(planetaRepository.findByNomeIgnoreCase(planeta.getNome())).thenReturn(planeta);

        mvc.perform(get("/planetas")
                .param("nome", "wookie"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void obterPlanetaPorId_RetornarPlaneta() throws Exception {
        Planeta planeta = new Planeta("utini", "clima", "terreno");
        planeta.setId("1234");

        when(planetaRepository.findById(planeta.getId())).thenReturn(java.util.Optional.of(planeta));
        when(planetaRepository.existsById(planeta.getId())).thenReturn(true);

        mvc.perform(get("/planetas/1234"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.nome", is(planeta.getNome())));
    }

    @Test
    public void obterPlanetaPorId_SeNaoHouverPlaneta_RetornarNotFound() throws Exception {
        Planeta planeta = new Planeta("utini", "clima", "terreno");
        planeta.setId("1234");

        when(planetaRepository.findById(planeta.getId())).thenReturn(java.util.Optional.of(planeta));
        when(planetaRepository.existsById(planeta.getId())).thenReturn(false);

        mvc.perform(get("/planetas/id_inexistente"))
                .andExpect(status().isNotFound());
    }
}