package desafio.b2w.api.service;

import desafio.b2w.api.entity.Planeta;
import desafio.b2w.api.repository.PlanetaRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class PlanetaServiceTest {

    @Mock
    private PlanetaRepository planetaRepository;

    @Mock
    private SwapiService swapi;

    @InjectMocks
    private PlanetaService service;

    @Test
    public void novo_SalvarPlanetaComQuantidadeDeAparicoes() {
        Planeta planeta = new Planeta("utini", "teste", "teste");
        when(swapi.obterAparicoesEmFilmes("utini")).thenReturn(123);

        service.novo(planeta);

        verify(planetaRepository, times(1)).save(planeta);
        Assert.assertEquals(123, planeta.getAparicoesEmFilmes());
    }
}