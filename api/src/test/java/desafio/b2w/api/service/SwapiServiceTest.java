package desafio.b2w.api.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SwapiServiceTest {

    @Spy
    private SwapiService service;

    private RestTemplate restMock;

    @Before
    public void setUp() throws Exception {
        restMock = mock(RestTemplate.class);
        when(service.getRestTemplate()).thenReturn(restMock);
    }

    @Test
    public void obterAparicoesEmFilmes_SeMapVazio_RetornaZero() {
        when(restMock.getForObject(anyString(), any())).thenReturn(new HashMap<>());

        int aparicoesEmFilmes = service.obterAparicoesEmFilmes("planeta");

        Assert.assertEquals(0, aparicoesEmFilmes);
    }

    @Test
    public void obterAparicoesEmFilmes_MapComCampoResultsNull_RetornaZero() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("results", null);
        when(restMock.getForObject(anyString(), any())).thenReturn(map);

        int aparicoesEmFilmes = service.obterAparicoesEmFilmes("planeta");

        Assert.assertEquals(0, aparicoesEmFilmes);
    }

    @Test
    public void obterAparicoesEmFilmes_MapComCampoResultsVazio_RetornaZero() {
        HashMap<String, Object> map = new HashMap<>();

        map.put("results", new ArrayList<>());
        when(restMock.getForObject(anyString(), any())).thenReturn(map);

        int aparicoesEmFilmes = service.obterAparicoesEmFilmes("planeta");

        Assert.assertEquals(0, aparicoesEmFilmes);
    }

    @Test
    public void obterAparicoesEmFilmes_MapComCampoResultsENenhumFilmeNaLista_RetornaZero() {
        HashMap<String, Object> map = new HashMap<>();

        ArrayList<Object> results = new ArrayList<>();
        HashMap<String, Object> planeta = new HashMap<>();
        List<String> films = new ArrayList<>();
        planeta.put("films", films);
        results.add(planeta);
        map.put("results", results);
        when(restMock.getForObject(anyString(), any())).thenReturn(map);

        int aparicoesEmFilmes = service.obterAparicoesEmFilmes("planeta");

        Assert.assertEquals(0, aparicoesEmFilmes);
    }

    @Test
    public void obterAparicoesEmFilmes_MapComCampoResultsEVariosFilmesNaLista_RetornaNumeroDeFilmes() {
        HashMap<String, Object> map = new HashMap<>();

        ArrayList<Object> results = new ArrayList<>();
        HashMap<String, Object> planeta = new HashMap<>();
        List<String> films = new ArrayList<>();
        films.add("Filme1");
        films.add("Filme2");
        planeta.put("films", films);
        results.add(planeta);
        map.put("results", results);
        when(restMock.getForObject(anyString(), any())).thenReturn(map);

        int aparicoesEmFilmes = service.obterAparicoesEmFilmes("planeta");

        Assert.assertEquals(2, aparicoesEmFilmes);
    }
}