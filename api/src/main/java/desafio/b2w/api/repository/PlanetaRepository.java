package desafio.b2w.api.repository;

import desafio.b2w.api.entity.Planeta;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlanetaRepository extends MongoRepository<Planeta, String> {

    Planeta findByNomeIgnoreCase(String nome);
}
