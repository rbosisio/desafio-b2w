package desafio.b2w.api.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "planetas")
public class Planeta {

    @Id
    private String id;

    private String nome;

    private String clima;

    private String terreno;

    private int aparicoesEmFilmes;

    public Planeta(String nome, String clima, String terreno) {
        this.nome = nome;
        this.clima = clima;
        this.terreno = terreno;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getClima() {
        return clima;
    }

    public void setClima(String clima) {
        this.clima = clima;
    }

    public String getTerreno() {
        return terreno;
    }

    public void setTerreno(String terreno) {
        this.terreno = terreno;
    }

    public int getAparicoesEmFilmes() {
        return aparicoesEmFilmes;
    }

    public void setAparicoesEmFilmes(int aparicoesEmFilmes) {
        this.aparicoesEmFilmes = aparicoesEmFilmes;
    }
}
