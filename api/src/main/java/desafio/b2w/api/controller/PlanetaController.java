package desafio.b2w.api.controller;

import desafio.b2w.api.entity.Planeta;
import desafio.b2w.api.repository.PlanetaRepository;
import desafio.b2w.api.service.PlanetaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("planetas")
public class PlanetaController {

    private Logger log = LoggerFactory.getLogger(PlanetaController.class);

    @Autowired
    private PlanetaRepository repository;

    @Autowired
    private PlanetaService service;


    @GetMapping(path = "", produces = "application/json")
    public ResponseEntity obterPlanetas(@RequestParam(name = "nome", required = false) String nome) {
        if (nome != null && !nome.isEmpty()) {
            log.info("GET /planetas?nome=" + nome);
            Planeta planeta = repository.findByNomeIgnoreCase(nome);
            if (planeta != null) return ResponseEntity.ok(planeta);
            else return ResponseEntity.notFound().build();
        }

        log.info("GET /planetas");
        List<Planeta> planetas = repository.findAll();
        return ResponseEntity.ok(planetas);
    }

    @GetMapping(path = "{id}", produces = "application/json")
    public ResponseEntity obterPlanetaPorId(@PathVariable(name = "id") String id) {
        log.info("GET /planetas/" + id);
        if (repository.existsById(id)) {
            return ResponseEntity.ok(repository.findById(id));
        }

        return ResponseEntity.notFound().build();
    }


    @PostMapping(path = "", consumes = "application/json", produces = "application/json")
    public ResponseEntity novoPlaneta(@RequestBody Planeta planeta) {
        log.info("POST /planetas");
        return ResponseEntity.ok(service.novo(planeta));
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity removerPlaneta(@PathVariable(name = "id", required = true) String id) {
        log.info("DELETE /planetas");
        repository.deleteById(id);
        return ResponseEntity.ok().build();
    }


}
