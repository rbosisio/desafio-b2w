package desafio.b2w.api.service;

import desafio.b2w.api.entity.Planeta;
import desafio.b2w.api.repository.PlanetaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlanetaService {

    private Logger log = LoggerFactory.getLogger(PlanetaService.class);

    @Autowired
    private PlanetaRepository repository;

    @Autowired
    private SwapiService swapi;

    public Planeta novo(Planeta planeta) {
        int aparicoesEmFilmes = swapi.obterAparicoesEmFilmes(planeta.getNome());
        planeta.setAparicoesEmFilmes(aparicoesEmFilmes);

        log.debug("Salvando planeta " + planeta.getNome() + " com " +aparicoesEmFilmes + " aparições em filmes");
        return repository.save(planeta);
    }
}
