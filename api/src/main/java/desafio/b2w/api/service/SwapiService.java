package desafio.b2w.api.service;

import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Map;

@Service
public class SwapiService {

    private Logger log = LoggerFactory.getLogger(SwapiService.class);

    public int obterAparicoesEmFilmes(String nomePlaneta) {
        RestTemplate rest = getRestTemplate();

        String url = "https://swapi.co/api/planets/?search=" + nomePlaneta;

        log.info("Obtedo informações da Swapi [url=" + url + "]");
        Map map = rest.getForObject(url, Map.class);

        List<Map> results = null;
        if (map.containsKey("results")) results = (List<Map>) map.get("results");
        List<String> films = null;
        if (results != null && results.size() > 0) films = (List<String>) results.get(0).get("films");

        if (films != null) return films.size();

        return 0;

    }

    protected RestTemplate getRestTemplate() {
        CloseableHttpClient httpClient
                = HttpClients.custom()
                .setSSLHostnameVerifier(new NoopHostnameVerifier())
                .build();
        HttpComponentsClientHttpRequestFactory requestFactory
                = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setHttpClient(httpClient);
        return new RestTemplate(requestFactory);
    }

}
