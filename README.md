## Desafio B2W 

Para rodar, basta executar o comando: **docker-compose up**

Caso queira rodar sem docker, basta executar da pasta do projeto o comando: **./gradlew bootRun**

A aplicação utiliza o MongoDB com endereço **localhost** , porta **27017** e o banco de dados **desafio**.

*Essas configurações de BD podem ser alteradas no application.properties*
